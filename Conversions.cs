﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise1
{
    class Conversions
    {
        static void Main(string[] args)
        {
            // Converting in Integer.
            Console.WriteLine("Enter string to convert into integer ");
            var input_int = Console.ReadLine();
            ConversionMethods.ConvertToInt(input_int);

            // For Float
            Console.WriteLine("Enter string to convert into float");
            string input_float = Console.ReadLine();
            ConversionMethods.ConvertToFloat(input_float);

            // For Boolean
            Console.WriteLine("Enter string to convert into boolean");
            string input_boolean = Console.ReadLine();
            ConversionMethods.ConvertToBoolean(input_boolean);
        }
    }
}
