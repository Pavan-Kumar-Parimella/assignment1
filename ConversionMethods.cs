﻿using System;

namespace Exercise1
{
    class ConversionMethods
    {
        /// <summary>
        /// Method to Convert string into integer.
        /// </summary>
        /// <param name="number"></param>
        public static void ConvertToInt(string number)
        {
            int int_converted_1 = int.Parse(number);
            int int_converted_2 = Convert.ToInt32(number);
            bool int_converted_3 = int.TryParse(number, out Int32 i);
            Console.WriteLine(int_converted_1 + " " + int_converted_2 + " " + int_converted_3);
        }

        /// <summary>
        /// Method to Convert a string into float.
        /// </summary>
        /// <param name="number"></param>
        public static void ConvertToFloat(string number)
        {
            bool float_converted = float.TryParse(number, out Single res);
            float float_converted_1 = Convert.ToSingle(number);
            float float_converted_2 = float.Parse(number);
            Console.WriteLine(float_converted + " " + float_converted_1 + " " + float_converted_2);
        }

        /// <summary>
        /// Method to Convert a string into Boolean.
        /// </summary>
        /// <param name="number"></param>
        public static void ConvertToBoolean(string number)
        {
            bool bool_converted = bool.TryParse(number, out Boolean res);

            Console.WriteLine(bool_converted);
        }
    }
}
