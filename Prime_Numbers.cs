﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Exercize3
{
    class PrimeNumber
    {
        /// <summary>
        /// Method to Print Prime Numbers within a given range.
        /// </summary>
        /// <param name="range1"></param>
        /// <param name="range2"></param>
        /// <param name="flag"></param>
        public static void Prime(int range1, int range2, int flag)
        {
            for (int i = range1; i <= range2; i++)
            {
                flag = 1;
                for (int j = 2; j < i; j++)
                {
                    if (i % j == 0)
                    {
                        flag = 0;
                    }
                }
                if (flag != 0)
                {
                    Console.WriteLine(i);
                }
            }
        }

        static void Main(string[] args)
        {
        again:

            int flag = 0;
            Console.WriteLine("Enter First Number");
            int number1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter Second Number");
            int number2 = int.Parse(Console.ReadLine());

            if (number1 > number2)
            {
                Console.WriteLine("Please Enter Again !! ");
                goto again;
            }
            else
            {
                Prime(number1, number2, flag);
            }
        }
    }
}
